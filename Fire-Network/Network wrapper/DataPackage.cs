﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network
{
    public class DataPackage
    {
        private bool _manipulated;
        public bool Manipulated => _manipulated;

        private PacketHeader _header;
        public PacketHeader Header => _header;

        private string _baseContent;
        public string Content => _baseContent;

        private byte[] _content;

        private byte[] _data;
        public byte[] Data
        {
            get
            {
                if (_data == null)
                    _data = getData();
                return _data;
            }
        }

        public DataPackage(string content, ProtocolInformation protocolInformation, int messageID = 0, uint messageNumber = 0)
        {
            _baseContent = content;
            _content = Encoding.ASCII.GetBytes(_baseContent);
            byte[] protocol = Encoding.ASCII.GetBytes(protocolInformation.ProtcolName);
            _header = new PacketHeader(_content, protocol, protocolInformation.ProtocolVersion, messageID, messageNumber);
        }

        public DataPackage(byte[] dataPackage)
        {
            _header = new PacketHeader(dataPackage);
            _content = new byte[_header.ContentSize];
            Buffer.BlockCopy(dataPackage, _header.HeaderSize, _content, 0, _header.ContentSize);
            _baseContent = Encoding.ASCII.GetString(_content);

            _manipulated = _header.isManipulated(_content);

        }

        private byte[] getData()
        {
            _header.getHeader();
            byte[] data = new byte[_header.CompleteSize];
            
            Buffer.BlockCopy(_header.Data, 0, data, 0, _header.HeaderSize);
            Buffer.BlockCopy(_content, 0, data, _header.HeaderSize, _header.ContentSize);
            return data;
        }
    }
}
