﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Collections;

namespace Fire_Network
{
    public class PacketHeader
    {
        private const int _protocolNameSize = 20;
        private const int _byteFields = 1;

        private int _headerSize;
        public int HeaderSize => _headerSize;

        private int _contentSize;
        public int ContentSize => _contentSize;

        private byte[] _protocolName;
        private Version _protocolVersion;

        public ProtocolInformation Protocol => new ProtocolInformation(Encoding.ASCII.GetString(_protocolName).Replace("\0", ""), _protocolVersion);

        private int _messageID;
        public int MessageID => _messageID;

        private uint _messageNumber;
        public uint MessageNumber => _messageNumber;

        public int CompleteSize => _headerSize + _contentSize;

        private bool _checksum;
        private byte[] _realChecksum;
        public byte[] CheckSum => _realChecksum;

        private bool _secureMessage;
        public bool MessageCopy => _secureMessage;
        private uint _copyOfMessageID;
        public uint CopyOf => _copyOfMessageID;



        //private bool _fragmented;
        //private byte[] _fragmentID;
        //private int _fragmentNumber
        //private bool _lastfragment


        private byte[] _data;
        public byte[] Data
        {
            get
            {
                if (_data == null)
                    _data = getHeader();
                return _data;
            }
        }

        public PacketHeader(byte[] content, byte[] protocolName, Version protocolVersion, int messageID, uint messageNumber)
        {
            _checksum = true;
            _contentSize = content.Length;
            _protocolName = new byte[_protocolNameSize];
            _messageID = messageID;
            _messageNumber = messageNumber;

            int runs = protocolName.Length > _protocolNameSize ? _protocolNameSize : protocolName.Length;
            for (int i = 0; i < runs; i++)
            {
                _protocolName[i] += protocolName[i];
            }

            _protocolVersion = protocolVersion;
            _realChecksum = calcChecksum(content);
        }

        internal void isMessageCopy(uint messageID)
        {
            _secureMessage = true;
            _copyOfMessageID = messageID;
        }

        public PacketHeader(byte[] content)
        {
            ReadHeader(content);
        }

        private void ReadHeader(byte[] header)
        {
            if (header.Length < sizeof(int) * 3 + 1)
                return;
            int currentStart = 0;

            _headerSize = BitConverter.ToInt32(getSubByteArray(header, currentStart, sizeof(int)), 0);
            currentStart += sizeof(int);

            _contentSize = BitConverter.ToInt32(getSubByteArray(header, currentStart, sizeof(int)), 0);
            currentStart += sizeof(int);

            _protocolName = getSubByteArray(header, currentStart, _protocolNameSize);
            currentStart += _protocolNameSize;

            _protocolVersion = getVersion(header, currentStart);
            currentStart += sizeof(int) * 4;

            _messageID = BitConverter.ToInt32(getSubByteArray(header, currentStart, sizeof(int)), 0);
            currentStart += sizeof(int);

            _messageNumber = BitConverter.ToUInt32(getSubByteArray(header, currentStart, sizeof(uint)), 0);
            currentStart += sizeof(uint);

            _copyOfMessageID = BitConverter.ToUInt32(getSubByteArray(header, currentStart, sizeof(uint)), 0);
            currentStart += sizeof(uint);

            byte[] booleanByte = getSubByteArray(header, currentStart, _byteFields);
            currentStart += _byteFields;

            BitArray booleans = new BitArray(booleanByte);

            _checksum = booleans[0];
            _secureMessage = booleans[1];

            if (_checksum)
            {
                int offset = currentStart;
                _realChecksum = new byte[_headerSize - offset];
                for (int i = offset; i < _headerSize; i++)
                {
                    int index = i - offset;
                    _realChecksum[index] = header[i];
                }
            }
        }

        private byte[] getSubByteArray(byte[] context, int start, int count)
        {
            byte[] returVal = new byte[count];
            for (int i = 0; i < count; i++)
            {
                int Index = i + start;
                returVal[i] = context[Index];
            }
            return returVal;
        }

        internal byte[] getHeader()
        {
            _headerSize = sizeof(int) * 4 + 1;
            _headerSize += _protocolName.Length;
            _headerSize += sizeof(int) * 4;



            if (_checksum)
                _headerSize += _realChecksum.Length;

            List<byte> header = new List<byte>();
            addByteArray(header, BitConverter.GetBytes(_headerSize));
            addByteArray(header, BitConverter.GetBytes(_contentSize));
            addByteArray(header, _protocolName);
            addByteArray(header, writeVersion());
            addByteArray(header, BitConverter.GetBytes(_messageID));
            addByteArray(header, BitConverter.GetBytes(_messageNumber));
            addByteArray(header, BitConverter.GetBytes(_copyOfMessageID));
            BitArray booleans = new BitArray(new bool[] {_checksum, _secureMessage, false, false });

            byte[] byteData = new byte[_byteFields];
            booleans.CopyTo(byteData, 0);
            addByteArray(header, byteData);

            if (_checksum)
                addByteArray(header, _realChecksum);

            return header.ToArray();
        }

        private byte[] writeVersion()
        {
            List<byte> version = new List<byte>();

            addByteArray(version, BitConverter.GetBytes(_protocolVersion.Major));
            addByteArray(version, BitConverter.GetBytes(_protocolVersion.Minor));
            addByteArray(version, BitConverter.GetBytes(_protocolVersion.Build));
            addByteArray(version, BitConverter.GetBytes(_protocolVersion.Revision));

            return version.ToArray();
        }

        private Version getVersion(byte[] header, int start)
        {
            
            byte[] versionBytes = getSubByteArray(header, start, sizeof(int) * 4);
            int major = BitConverter.ToInt32(getSubByteArray(versionBytes, 0, sizeof(int)), 0);
            int minor = BitConverter.ToInt32(getSubByteArray(versionBytes, sizeof(int), sizeof(int)), 0);
            int build = BitConverter.ToInt32(getSubByteArray(versionBytes, sizeof(int) * 2, sizeof(int)), 0);
            int revision = BitConverter.ToInt32(getSubByteArray(versionBytes, sizeof(int) * 3, sizeof(int)), 0);
            return new Version(major, minor, build, revision);
        }

        private void addByteArray(List<byte> target, byte[] newBytes)
        {
            for (int i = 0; i < newBytes.Length; i++)
            {
                target.Add(newBytes[i]);
            }
        }

        private byte[] calcChecksum(byte[] content)
        {
            using (MD5 md5 = MD5.Create())
            {
                return md5.ComputeHash(content);
            }
        }

        public bool isManipulated(byte[] content)
        {
            if (_checksum == false)
                return false;
            byte[] checksum = calcChecksum(content);
            if (_realChecksum.Length != checksum.Length)
                return true;

            for (int i = 0; i < checksum.Length; i++)
            {
                if (_realChecksum[i] != checksum[i])
                    return true;
            }

            return false;
        }

        
    }
}
