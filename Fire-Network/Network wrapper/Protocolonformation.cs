﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network
{
    public class ProtocolInformation
    {
        private Version _protocolVersion;
        public Version ProtocolVersion => _protocolVersion;

        private string _protocolName;
        public string ProtcolName => _protocolName;

        public ProtocolInformation(string name, Version version)
        {
            _protocolName = name;
            _protocolVersion = version;
        }
        
    }
}
