﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network.Network_wrapper
{
    public class DataEvent : EventArgs
    {
        private DataPackage _data;
        public DataPackage Data => _data;

        private IPEndPoint _messageSender;
        public IPAddress IP => _messageSender.Address;
        public int Port => _messageSender.Port;


        public DataEvent(DataPackage data, IPEndPoint sender)
        {
            _data = data;
            _messageSender = sender;
        }
    }
}
