﻿using Fire_Network.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network.UDP
{
    public class BasicUDPClient : BasicUDPNetwork
    {
        private IPEndPoint _bindEndPoint;
        public bool ThrowAwayOldMessage
        {
            get
            {
                return _throwAwayOldMessages;
            }
            set
            {
                _throwAwayOldMessages = value;
            }
        }

        public BasicUDPClient(ProtocolInformation protocol, IPEndPoint localEndPoint = null) : base(protocol)
        {
            _bindEndPoint = null;
        }

        public bool Initialize(IPEndPoint IPLocalEndPoint = null)
        {
            if (IPLocalEndPoint != null)
                _localEndpoint = IPLocalEndPoint;
            else
            {
                _localEndpoint = new IPEndPoint(GetLocalIPv4Address(), GetFreePort());
            }

            _localClient = new UdpClient(_localEndpoint);
            _localClient.BeginReceive(new AsyncCallback(ReciveData), _localClient);


            initialized = true;
            return true;
        }

        private bool checkSending(ref IPEndPoint target)
        {
            if (_bindEndPoint != null)
                target = _bindEndPoint;

            if (target == null)
            {
                TriggerErrorEvent("Please enter a target point or bind the client to a endpoint!", ErrorLevel.Error);
                return false;
            }
            return true;
        }

        public void SendSecureMessage(string Message, int MessageID, IPEndPoint target = null)
        {
            if (!checkSending(ref target))
                return;

            secureSendData(Message, MessageID, target);
        }

        public void SendMessage(string Message, int MessageID, IPEndPoint target = null)
        {
            if (!checkSending(ref target))
                return;

            sendData(Message, MessageID, target);
        }

        public void SendBroadcast(string Message, int MessageID, int port, bool secure = false)
        {
            IPEndPoint target = new IPEndPoint(IPAddress.Broadcast, port);
            if (!secure)
                SendMessage(Message, MessageID, target);
            else
                SendSecureMessage(Message, MessageID, target);
        }

        public void bindToIP(IPEndPoint target)
        {
            _bindEndPoint = target;
        }
    }
}
