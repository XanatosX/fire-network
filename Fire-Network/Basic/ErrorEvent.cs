﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network.Basic
{
    public enum ErrorLevel
    {
        Unknown = 404,
        Critical = 1,
        Error = 2,
        Warning = 3,

    }

    public class NetworkError : EventArgs
    {
        private ErrorLevel _level;
        public ErrorLevel Level => _level;

        private string _message;
        public String Message => _message;

        public NetworkError(string message, ErrorLevel level = ErrorLevel.Unknown)
        {
            _level = level;
            _message = message;
        }
    }
}
