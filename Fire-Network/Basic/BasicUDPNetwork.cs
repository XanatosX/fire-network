﻿using Fire_Network.Basic;
using Fire_Network.Network_wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network
{
    public class BasicUDPNetwork : BasicNetwork
    {
        protected const int _secureCopies = 30;
        protected bool _throwAwayOldMessages;

        protected UdpClient _localClient;
        private uint _messageNumber;
        private uint _lastMessage;

        private bool firstMessage;

        public event EventHandler<DataEvent> MessageRecived;

        public BasicUDPNetwork(ProtocolInformation protocol) : base(protocol)
        {
            _messageNumber = uint.MinValue + 1;
            _lastMessage = uint.MinValue;
            firstMessage = true;
            _throwAwayOldMessages = true;
        }

        

        internal void TriggerDataRecivedEvent(DataPackage data, IPEndPoint SendetClient)
        {
            TriggerDataRecivedEvent(new DataEvent(data, SendetClient));
        }

        internal void TriggerDataRecivedEvent(DataEvent data)
        {
            MessageRecived?.Invoke(this, data);
        }

        private DataPackage createDataPackage(string Message, int messageID, uint copyOf = uint.MinValue)
        {
            if (_messageNumber == uint.MaxValue)
            {
                _messageNumber = uint.MinValue + 1;
            }
            DataPackage returnPackage = new DataPackage(Message, _protocol, messageID, _messageNumber);
            if (copyOf != uint.MinValue)
                returnPackage.Header.isMessageCopy(copyOf);
           _messageNumber++;

            return returnPackage;
        }

        internal void secureSendData(string Message, int messageID, IPEndPoint targetClient)
        {
            uint mainMessageNumber = _messageNumber;
            for (int i = 0; i < _secureCopies; i++)
            {
                DataPackage package = createDataPackage(Message, messageID);
                package.Header.isMessageCopy(mainMessageNumber);
                sendData(package, targetClient);
            }
        }
        internal void sendData(string Message, int messageID, IPEndPoint targetClient)
        {
            DataPackage package = createDataPackage(Message, messageID);
            sendData(package, targetClient);
        }
        private void sendData(DataPackage package, IPEndPoint targetClient)
        {
            if (!initialized)
            {
                TriggerErrorEvent("Is not initialized yet!", Basic.ErrorLevel.Critical);
                return;
            }
            try
            {
                _localClient.BeginSend(package.Data, package.Data.Length, targetClient, new AsyncCallback(SendCallback), _localClient);
            }
            catch (Exception ex)
            {
                TriggerErrorEvent(ex.Message, Basic.ErrorLevel.Error);
            }

        }
        private void SendCallback(IAsyncResult ar)
        {
            UdpClient u = (UdpClient)ar.AsyncState;
        }

        public void ReciveData(IAsyncResult ar)
        {
            UdpClient uc = ((UdpClient)ar.AsyncState);
            IPEndPoint LocalIPEndPoint = _localEndpoint;

            try
            {
                byte[] recevied = _localClient.EndReceive(ar, ref LocalIPEndPoint);
                DataPackage data = new DataPackage(recevied);
                if (!ProtocolOkay(data.Header.Protocol))
                    return;
                if (_throwAwayOldMessages && data.Header.MessageNumber < _lastMessage && !firstMessage)
                    return;

                firstMessage = false;

                _lastMessage = data.Header.MessageNumber;
                TriggerDataRecivedEvent(data, LocalIPEndPoint);
                _localClient.BeginReceive(new AsyncCallback(ReciveData), _localClient);
            }
            catch (Exception ex) 
            {
                TriggerErrorEvent(ex.Message, ErrorLevel.Error);
            }
        }

        private bool ProtocolOkay(ProtocolInformation protocol)
        {
            if (protocol.ProtcolName != _protocol.ProtcolName || protocol.ProtocolVersion != _protocol.ProtocolVersion)
                return false;
            return true;
        }
    }
}
