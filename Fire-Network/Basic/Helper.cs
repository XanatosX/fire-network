﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network
{
    public static class NetworkHelper
    {
        public static string getMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            List<PhysicalAddress> addresses = new List<PhysicalAddress>();
            foreach (NetworkInterface nic in nics)
            {
                if (nic.OperationalStatus != OperationalStatus.Up || nic.NetworkInterfaceType == NetworkInterfaceType.Loopback || nic.NetworkInterfaceType == NetworkInterfaceType.Tunnel)
                    continue;
                PhysicalAddress address = nic.GetPhysicalAddress();
                addresses.Add(address);
            }

            if (addresses.Count == 1)
                return addresses[0].ToString();

            return string.Empty;
        }
    }
}
