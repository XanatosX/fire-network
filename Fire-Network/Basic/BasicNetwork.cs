﻿using Fire_Network.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Fire_Network
{
    public class BasicNetwork
    {
        protected bool initialized;
        protected ProtocolInformation _protocol;
        protected IPEndPoint _localEndpoint;

        public int Port => _localEndpoint.Port;
        public IPAddress IP => _localEndpoint.Address;

        public event EventHandler<NetworkError> Error;

        public BasicNetwork(ProtocolInformation protocol)
        {
            _protocol = protocol;
        }

        protected void TriggerErrorEvent(string Message, ErrorLevel level = ErrorLevel.Unknown)
        {
            TriggerErrorEvent(new NetworkError(Message, level));
        }

        protected void TriggerErrorEvent(NetworkError error)
        {
            Error?.Invoke(this, error);
        }

        internal IPAddress GetIPFromString(string IPString, out IPAddress IP)
        {
            if (!IPAddress.TryParse(IPString, out IP))
            {
                return null;
            }

            return IP;
        }

        public IPAddress GetLocalIPv4Address()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            for (int i = 0; i < host.AddressList.Length; i++)
            {
                IPAddress CurrentIP = host.AddressList[i];
                if (CurrentIP.AddressFamily == AddressFamily.InterNetwork)
                {
                    return CurrentIP;
                }
            }
            return null;
        }

        public int GetFreePort(int MinPort = 3000)
        {
            int CurrentPort = MinPort + 1;
            while (portIsBlocked(CurrentPort))
            {
                CurrentPort++;
            }
            return CurrentPort;
        }

        protected bool portIsBlocked(int Port)
        {
            bool blocked = false;

            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] UDPEndPoints = ipGlobalProperties.GetActiveUdpListeners();

            foreach (IPEndPoint tcpi in UDPEndPoints)
            {
                if (tcpi.Port == Port)
                {
                    blocked = true;
                    break;
                }
            }

            return blocked;
        }
        
    }
}
