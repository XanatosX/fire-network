﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Fire_Network;

namespace FireNetworkTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void HeaderTest()
        {
            string content = "Some really nice DataPackage";
            string ProtocolName = "Test";
            Version ProtocolVersion = new Version(4, 3, 2, 1);

            ProtocolInformation protocolInformation = new ProtocolInformation(ProtocolName, ProtocolVersion);

            int MessageID = 2;
            uint MessageNumber = 3;
            DataPackage data = new DataPackage(content, protocolInformation, MessageID, MessageNumber);

            byte[] dataTest = data.Data;

            DataPackage testDataPackage = new DataPackage(dataTest);
            PacketHeader header = testDataPackage.Header;

            Assert.AreEqual(content, testDataPackage.Content);
            Assert.AreEqual(ProtocolName, header.Protocol.ProtcolName);
            Assert.AreEqual(ProtocolVersion, header.Protocol.ProtocolVersion);
            Assert.AreEqual(MessageID, header.MessageID);
            Assert.AreEqual(MessageNumber, header.MessageNumber);
            Assert.AreEqual(false, testDataPackage.Manipulated);

        }
    }
}
