﻿using Fire_Network;
using Fire_Network.UDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(NetworkHelper.getMACAddress());
            
            ProtocolInformation testProtocol = new ProtocolInformation("Test", new Version(4, 3, 2, 1));
            DataPackage data = new DataPackage("Some really nice DataPackage", testProtocol, 2, 3);
            byte[] dataTest =  data.Data;

            DataPackage testData = new DataPackage(data.Data);
            PacketHeader header = testData.Header;

            Console.WriteLine($"MessageID: {header.MessageID}");
            Console.WriteLine($"MessageNumber: {header.MessageNumber}");
            Console.WriteLine($"Headersize: {header.HeaderSize}");
            Console.WriteLine($"Contentsize: {header.ContentSize}");
            Console.WriteLine($"Manipulated: {testData.Manipulated.ToString()}");
            Console.WriteLine($"Message copy: {header.MessageCopy.ToString()}");
            Console.WriteLine($"Message copyof: {header.CopyOf.ToString()}");
            Console.WriteLine($"ProtocolName: {header.Protocol.ProtcolName}");
            Console.WriteLine($"ProtocolVersion: {header.Protocol.ProtocolVersion}");

            BasicUDPClient TestSender = new BasicUDPClient(testProtocol);
            TestSender.Error += TestSender_Error;
            TestSender.Initialize();

            BasicUDPClient TestRecivier = new BasicUDPClient(testProtocol);
            TestRecivier.Error += TestSender_Error;
            TestRecivier.Initialize();
            TestRecivier.MessageRecived += TestRecivier_MessageRecived;


            Console.ReadLine();
            Console.Clear();
            TestSender.SendBroadcast("BroadcastTest", 150, 3001);
            Console.ReadLine();
            Console.Clear();
            TestSender.SendBroadcast("BroadcastSecureTest", 150, 3001, true);
            Console.ReadLine();

            string message = "";
            while (message.ToLower() != "exit")
            {
                Console.Clear();
                message = Console.ReadLine();
                if (message.ToLower().Contains("secure"))
                    TestSender.SendSecureMessage(message, 100, new IPEndPoint(TestRecivier.IP, TestRecivier.Port));
                else
                    TestSender.SendMessage(message, 100, new IPEndPoint(TestRecivier.IP, TestRecivier.Port));
            }

            Console.ReadLine();
        }

        private static void TestSender_Error(object sender, Fire_Network.Basic.NetworkError e)
        {
            Console.WriteLine($"{e.Level.ToString()}: {e.Message}");
        }

        private static void TestRecivier_MessageRecived(object sender, Fire_Network.Network_wrapper.DataEvent e)
        {
            DataPackage dataPackage = e.Data;
            PacketHeader header = dataPackage.Header;

            Console.WriteLine("Recived");
            Console.WriteLine($"MessageID: {header.MessageID}");
            Console.WriteLine($"MessageNumber: {header.MessageNumber}");
            Console.WriteLine($"Headersize: {header.HeaderSize}");
            Console.WriteLine($"Contentsize: {header.ContentSize}");
            Console.WriteLine($"Manipulated: {dataPackage.Manipulated.ToString()}");
            Console.WriteLine($"Message copy: {header.MessageCopy.ToString()}");
            Console.WriteLine($"Message copyof: {header.CopyOf.ToString()}");
            Console.WriteLine($"ProtocolName: {header.Protocol.ProtcolName}");
            Console.WriteLine($"ProtocolVersion: {header.Protocol.ProtocolVersion}");
            Console.WriteLine($"Message Content {dataPackage.Content}");
        }
    }
}
